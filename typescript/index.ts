export enum PriorityEnum {
  HIGH = 'HIGH',
  LOW = 'LOW',
  MEDIUM = 'MEDIUM'
}

export enum RoleEnum {
  ADMIN,
  STANDARD
}

export interface User {
  id: string,
  email?: string,
  name?: string,
  role: RoleEnum,
  tasks: Task[]
}

export interface Task {
  id?: string,
  description?: string,
  priority?: PriorityEnum,
  owner?: User
}