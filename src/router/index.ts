import { createRouter, createWebHistory } from 'vue-router'
import HomeView from '../views/HomeView.vue'

export const routes = [
  {
    path: '/',
    name: 'home',
    component: HomeView,
    meta: {
      name: 'Home'
    }
  },
  {
    path: '/queries/',
    name: 'queries',
    component: () => import('../views/queries/QueriesView.vue'),
    meta: { name: 'Queries' },
    children: [
      {
        path: '',
        name: 'select',
        component: () => import('../views/queries/SimpleQueryView.vue'),
        meta: {
          name: 'Select'
        }
      },
      {
        path: 'double',
        name: 'double',
        component: () => import('../views/queries/DoubleQueryView.vue'),
        meta: {
          name: 'Multiple'
        }
      },
      {
        path: 'filter',
        name: 'filter',
        component: () => import('../views/queries/FilterQuery.vue'),
        meta: {
          name: 'Filter'
        }
      },
      {
        path: 'filterUI',
        name: 'filterUI',
        component: () => import('../views/queries/FilterQueryUI.vue'),
        meta: {
          name: 'Filter UI'
        }
      },
      {
        path: 'find',
        name: 'find',
        component: () => import('../views/queries/FindQuery.vue'),
        meta: {
          name: 'Find'
        }
      },
      {
        path: 'subselection',
        name: 'subselection',
        component: () => import('../views/queries/SubSelectionView.vue'),
        meta: {
          name: 'Sub-selection'
        }
      },
      {
        path: 'exercise',
        name: 'exercise',
        component: () => import('../views/queries/ExercisesQuery.vue'),
        meta: {
          name: 'Exercise'
        }
      }
    ]
  },
  {
    path: '/mutations/',
    name: 'mutations',
    component: () => import('../views/mutations/MutationsView.vue'),
    meta: { name: 'Mutations' },
    children: [
      {
        path: '',
        name: 'simple',
        component: () => import('../views/mutations/SimpleMutation.vue'),
        meta: {
          name: 'Simple mutation'
        }
      }
    ]
  }
]
const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: routes
})

router.afterEach((to) => (document.title = to.meta?.name + ' | GraphQL'))
export default router
